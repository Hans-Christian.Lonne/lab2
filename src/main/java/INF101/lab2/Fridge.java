package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
	
	private List<FridgeItem> items;
	
	int maxSize = 20;
	
	
	public Fridge() {
		items = new ArrayList<FridgeItem>();
	}
	
	public int totalSize( ) {
		return maxSize;
	}

	@Override
	public int nItemsInFridge() {
		return items.size();
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (item == null) {
			throw new IllegalArgumentException("Cannot place 'null' in fridge");
		}
		if (items.size() >= maxSize) {
			return false;
		}
		items.add(item);
		return true;
	}

	@Override
	public void takeOut(FridgeItem item) {
		if (item == null) {
			throw new IllegalArgumentException("Cannot remove 'null' from fridge");
		}
		if (!items.contains(item) ) {
			throw new NoSuchElementException("Item not in fridge");
		}
		items.remove(item);
	}

	@Override
	public void emptyFridge() {
		items.clear();	
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
		for (FridgeItem item : items) {
			if (item.hasExpired()) {
				expiredItems.add(item);
			}
		}
		for (FridgeItem item : expiredItems) {
			items.remove(item);
		}
		return expiredItems;
	}

}
